function activeSurvey(sid,expire)
{
  var token= sessionStorage.getItem("token");
 $("#"+sid).html("Please wait..");
$.post("active_survey.php",
  {
    sessionKey: token,
    sid: sid,
    expire: expire
  },
  function(data, status){
    
  });
}


function getExport(sid)
{
  var token= sessionStorage.getItem("token");
  
  window.open(
    "export_response.php?sid="+sid+"&code="+token,
    '_blank'
  );
}


function getReport(sid)
{
  var token= sessionStorage.getItem("token");
  
  window.open(
    "report_response.php?sid="+sid+"&code="+token,
    '_blank'
  );
}
function getSummary(sid)
{
  var token= sessionStorage.getItem("token");
 $("#"+sid).html("Please wait..");
$.post("get_survey_summary.php",
  {
    sessionKey: token,
    sid: sid
  },
  function(data, status){
    $("#"+sid).html("");

    $.each(data, function(k, v) {
        
        $("#"+sid).append(' <b>'+k + '</b> = ' + v+' ');
    });   
  });
}
function loadSurveys()
{
    $('#survys').html('Please wait..');
    $.get( "surveys.php", function( data ) {
        var survys = data.surveys;
        sessionStorage.setItem("token", data.token);

        $('#survys').append('<ul class="list-group"><li class="list-group-item active">Surveys</li>');
        for (var i = 0; i < survys.length; i++) {
            var element = survys[i];
            
            
        var status ="Expire";
        var msg ="Expired"
        var expire = 1;
        if(element.expires === null || element.expires === undefined)
        {
           msg="Live"; 
        }
        else
        {
         msg ="Expired on "+element.expires;
         status="Start Again"
         expire = 0;

        }
            
            $('#survys').append("<li class='list-group-item'><p>"
             + element.surveyls_title + " <br> <small> <span id='"+element.sid+"'>"+msg+"</span></small></p>"+
             " <button type='button' class='btn btn-outline-success btn-sm' onclick='activeSurvey("+element.sid+","+expire+")'>"+status+"</button>"+
             " <button type='button' class='btn btn-outline-success btn-sm' onclick='getSummary("+element.sid+")'>Summary</button>"+
             " <button type='button' class='btn btn-outline-success btn-sm' onclick='getExport("+element.sid+")'>Export Responses</button>"+
             " <button type='button' class='btn btn-outline-warning btn-sm' onclick='getReport("+element.sid+")'>Report</button>"+

             " <a class='btn btn-outline-primary btn-sm' target='_blank' href='https://socialnama.limequery.com/"+element.sid+"?lang=en'>Preview</a></li>");
       }
       $('#survys').append('</ul>');

      });
}
