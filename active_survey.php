<?php
include_once 'vendor/autoload.php';

include 'config.php';



$myJSONRPCClient = new \org\jsonrpcphp\JsonRPCClient( LS_BASEURL.'/admin/remotecontrol' );

$sessionKey = $_POST["sessionKey"];
$sid = $_POST["sid"];
$expire = $_POST["expire"];


//echo "Session Key".$sessionKey;
//$response=$myJSONRPCClient->activate_survey($sessionKey,$sid);

if(!$expire)
{
    $date=date('Y-m-d 00:00:00');
$aSurveyData = array('expires'=>null);
$response = $myJSONRPCClient->set_survey_properties($sessionKey, $sid, $aSurveyData);
}
else
{
$date=date('Y-m-d 00:00:00');
$aSurveyData = array('expires'=>'2020-01-01 00:00:00');
$response = $myJSONRPCClient->set_survey_properties($sessionKey, $sid, $aSurveyData);
}

header('Content-Type: application/json');
echo json_encode($response);
?>
